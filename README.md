# Python EOQ Web Server - pyeoqwebserver

The pyeoqwebserver creates a local EOQ workspace model domain from a local directory. The interface to that domain is made accessible by http post and web sockets. Multiple clients can connect to the same server. Events are only available through the web socket interface. By default the server will wait for JSON serialized EOQ frames on:

* http://localhost:8000/ws/eoq.do (HTTP post)
* ws://localhost:8000/ws/eoq.do (web socket)

The web server can be started and configured using the command as follows: 

    python PyEoq2WebServer.py --port 8000 --workspaceDir "./Workspace" 
	--metaDir "Meta" --actions 1 --actionsDir "./Actions" 
	--backup 1 --backupDir "./backup" --logDir "./log" 
	--logToConsole 1 --autosave 5.0 --trackFileChanges 1

The [Test](https://gitlab.com/eoq/tools/pyeoqwebserver/-/tree/master/Test) folder contains an out-of-the-box start script as well as a workspace. Use this for testing.

The pyeoqwebserver is based on [PyEOQ](https://gitlab.com/eoq/py/pyeoq). 

The webserver requires [Tornado](https://www.tornadoweb.org) for Python to be installed.

# EOQ

[Essential Object Query (EOQ)](https://gitlab.com/eoq/essentialobjectquery) is a language to interact remotely and efficiently with object-oriented models, i.e. domain-specific models. It explicitly supports the search for patterns, as used in model transformation languages. Its motivation is an easy to parse and deterministically behaving query structure, but as high as possible efficiency and flexibility. EOQ’s capabilities and semantics are similar to the Object-Constraint-Language (OCL), but it supports in addition transactional model modification, change events, and error handling.  

Main Repository: https://gitlab.com/eoq/essentialobjectquery

EOQ user manual: https://gitlab.com/eoq/doc 

