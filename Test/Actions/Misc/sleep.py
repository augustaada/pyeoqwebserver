'''
    An action to slow down the query execution 
'''

import time

def sleep(domain : 'Domain', seconds : 'Real=1.0:The sleep time in seconds'):
    time.sleep(seconds)
    return